"""
cvssv2 parser's class containting definitions used in main class
"""

from decimal import Decimal as Dec

ROUND_IN_CALCULATION = True
ROUND_EXP = 1
DEBUG = False


class Vector(object):
    def __init__(self, name, values_list, values):
        self.name = name
        self.values_list = values_list
        self.values = values


ACCESS_VECTOR = Vector(name="AccessVector",
                       values_list="[L]ocal/[A]djacent/[N]etwork",
                       values={"L": Dec("0.395"), "A": Dec("0.646"), "N": Dec("1.0")})
ACCESS_COMPLEXITY = Vector(name="AccessComplexity",
                           values_list="[H]igh/[M]edium/[L]ow",
                           values={"H": Dec("0.35"), "M": Dec("0.61"), "L": Dec("0.71")})
AUTHENTICATION = Vector(name="Authentication",
                        values_list="[M]ultiple instances/[S]ingle instance/[N]o authentication",
                        values={"M": Dec("0.45"), "S": Dec("0.56"), "N": Dec("0.704")})
CONFIDENTIALITY_IMPACT = Vector(name="ConfidentialityImpact",
                                values_list="[N]one/[P]artial/[C]omplete",
                                values={"N": Dec("0.0"), "P": Dec("0.275"), "C": Dec("0.660")})
INTEGRITY_IMPACT = Vector(name="IntegrityImpact",
                          values_list="[N]one/[P]artial/[C]omplete",
                          values={"N": Dec("0.0"), "P": Dec("0.275"), "C": Dec("0.660")})
AVAIL_IMPACT = Vector(name="AvailImpact",
                      values_list="[N]one/[P]artial/[C]omplete",
                      values={"N": Dec("0.0"), "P": Dec("0.257"), "C": Dec("0.660")})


# Temporar
EXPPLOITABILITY = Vector(name="Exploitability",
                         values_list="[U]nproven/[P]roof of Concept/[F]unctional/[H]igh/[N]ot defined",
                         values={"U": Dec("0.85"), "P": Dec("0.9"), "F": Dec("0.95"), "H": Dec("1.0"),
                                 "N": Dec("1.0")})
REMEDATION_LEVEL = Vector(name="RemediationLevel",
                          values_list="[O]fficial-fix/[T]emporary-fix/[W]orkaround/[U]navailable/[N]ot defined",
                          values={"O": Dec("0.87"), "T": Dec("0.9"), "W": Dec("0.95"), "U": Dec("1.0"),
                                  "N": Dec("1.0")})
REPORT_CONFIDENCE = Vector(name="ReportConfidence",
                           values_list="[U]nconfirmed/Uncorro[B]ated/[C]onfirmed/[N]ot defined",
                           values={"U": Dec("0.90"), "B": Dec("0.95"), "C": Dec("1.00"),
                                   "N": Dec("1.00")})


# Environmental
COLLATERAL_DMG_POTENTIAL = Vector(name="CollateralDamagePotential",
                                  values_list="[N]one/[L]ow/[LM]=Low - medium/" +
                                              "[MH]=Medium - high/[H]igh/[ND]=Not defined",
                                  values={"N": Dec("0.0"), "L": Dec("0.1"), "LM": Dec("0.3"),
                                          "MH": Dec("0.4"), "H": Dec("0.5"), "ND": Dec("0.0")})
TARGET_DISTRIBUTION = Vector(name="TargetDistribution",
                             values_list="[N]one/[L]ow/[M]edium/[H]igh/[ND]=Not defined",
                             values={"N": 0, "L": Dec("0.25"), "M": Dec("0.75"),
                                     "H": Dec("1.00"), "ND": Dec("1.00")})
CONF_REQ = Vector(name="ConfReq",
                  values_list="[L]ow/[M]edium/[H]igh/[ND]=Not defined",
                  values={"L": Dec("0.5"), "M": Dec("1.0"),
                          "H": Dec("1.51"), "ND": Dec("1.0")})
INTEG_REQ = Vector(name="IntegReq",
                   values_list="[L]ow/[M]edium/[H]igh/[ND]=Not defined",
                   values={"L": Dec("0.5"), "M": Dec("1.0"),
                           "H": Dec("1.51"), "ND": Dec("1.0")})
AVAIL_REQ = Vector(name="AvailReq",
                   values_list="[L]ow/[M]edium/[H]igh/[ND]=Not defined",
                   values={"L": Dec("0.5"), "M": Dec("1.0"),
                           "H": Dec("1.51"), "ND": Dec("1.0")})


class Metric(object):
    def __init__(self, vectors):
        self.vectors = vectors


Base = Metric([ACCESS_VECTOR, ACCESS_COMPLEXITY, AUTHENTICATION,
               CONFIDENTIALITY_IMPACT, INTEGRITY_IMPACT, AVAIL_IMPACT])
Temporar = Metric([EXPPLOITABILITY, REMEDATION_LEVEL, REPORT_CONFIDENCE])
Environmental = Metric([COLLATERAL_DMG_POTENTIAL, TARGET_DISTRIBUTION, CONF_REQ, INTEG_REQ, AVAIL_REQ])


class Equations(object):
    """
    Class for keeping all equations needed for cvss2
    """

    @staticmethod
    def base_equation(acces_vector: Dec, access_complexity,
                      authentication, conf_impact, integ_impact, avail_impact):
        impact = Dec("10.41") * (1 - (1 - conf_impact) * (1 - integ_impact) * (1 - avail_impact))

        exploitability = Dec("20") * acces_vector * access_complexity * authentication

        f = 0 if impact == 0 else Dec("1.176")
        score = ((Dec("0.6") * impact) + (Dec("0.4") * exploitability) - Dec("1.5")) * f
        return round(score, ROUND_EXP)

    @staticmethod
    def temporar_equation(base_score, exploitability, remediation_level, report_confidence):
        score = base_score * exploitability * remediation_level * report_confidence
        return round(score, ROUND_EXP)

    @staticmethod
    def environmental_equation(target_distribution, conf_req, integ_req, avail_req,
                               conf_impact, integ_impact, exploitability, remediation_level, report_confidence,
                               avail_impact, collateral_damage_potential,
                               acces_vector, access_complexity, authentication):
        adjusted_impact = Dec.min(Dec("10"),
                                  Dec("10.41") * (Dec("1") -
                                                  (Dec("1") - conf_impact * conf_req) *
                                                  (Dec("1") - integ_impact * integ_req) *
                                                  (Dec("1") - avail_impact * avail_req)
                                                  ))
        f = 0 if adjusted_impact == 0 else Dec("1.176")
        exploitab = Dec("20") * acces_vector * access_complexity * authentication
        adjusted_base = ((Dec("0.6") * adjusted_impact) + (Dec("0.4") * exploitab) - Dec("1.5")) * f

        adjusted_temporar = adjusted_base * exploitability * remediation_level * report_confidence

        score = (adjusted_temporar + (Dec("10") - adjusted_temporar) *
                 collateral_damage_potential) * target_distribution

        return round(score, ROUND_EXP)


def calculate_results(vect, temporary_eq=False, environmental_eq=False):
    """
    Method for calculation of cvss-v2 score. First, calculates base_equation. Then, if in Temporary and Environmental
     variables are enabled,
    :param vect:
    :return: dec - calculated score
    """
    base_score = Equations.base_equation(acces_vector=vect[ACCESS_VECTOR.name],
                                         access_complexity=vect[ACCESS_COMPLEXITY.name],
                                         authentication=vect[AUTHENTICATION.name],
                                         conf_impact=vect[CONFIDENTIALITY_IMPACT.name],
                                         integ_impact=vect[INTEGRITY_IMPACT.name],
                                         avail_impact=vect[AVAIL_IMPACT.name])
    score = base_score

    if temporary_eq:
        score = Equations.temporar_equation(base_score, exploitability=vect[EXPPLOITABILITY.name],
                                            remediation_level=vect[REMEDATION_LEVEL.name],
                                            report_confidence=vect[REPORT_CONFIDENCE.name])

    if environmental_eq:
        score = Equations.environmental_equation(target_distribution=vect[TARGET_DISTRIBUTION.name],
                                                 avail_impact=vect[AVAIL_IMPACT.name],
                                                 avail_req=vect[AVAIL_REQ.name],
                                                 collateral_damage_potential=vect[COLLATERAL_DMG_POTENTIAL.name],
                                                 conf_impact=vect[CONFIDENTIALITY_IMPACT.name],
                                                 conf_req=vect[CONF_REQ.name],
                                                 exploitability=vect[EXPPLOITABILITY.name],
                                                 integ_impact=vect[INTEGRITY_IMPACT.name],
                                                 integ_req=vect[INTEG_REQ.name],
                                                 remediation_level=vect[REMEDATION_LEVEL.name],
                                                 report_confidence=vect[REPORT_CONFIDENCE.name],
                                                 access_complexity=vect[ACCESS_COMPLEXITY.name],
                                                 acces_vector=vect[ACCESS_VECTOR.name],
                                                 authentication=vect[AUTHENTICATION.name])

    return score
