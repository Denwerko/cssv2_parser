#!/bin/env python3
"""
cvssv2 parser
"""
import functools
import argparse

from cvssv2 import *

__author__ = 'Stefan Porhincak'

# Base

parser = argparse.ArgumentParser(description='cvssv2 parser')
_TEMPORAR = 'Temporar'
_ENVIRONMENTAL = 'Environmental'


def _help_msg(vect):
    """
    Helper method
    :param vect: Vector object
    :return:
    """

    def concat_msg(x, y=None):
        return "{},{}".format(x, y)

    if type(vect) is Vector:
        return "{}:case of {}. Valid inputs: {}".format(vect.name, vect.values_list, "".join(
            [str(x) for x in list(functools.reduce(concat_msg, list(vect.values.keys())))]))


def _help_param(vect):
    """
    Helper method
    :param vect: Vector object
    :return:
    """

    return {'type': str, 'help': _help_msg(vect), 'dest': vect.name}


def parser_init():
    parser.add_argument('-I', help="Interactive input", action='store_true',
                        dest="Interactive")
    parser.add_argument('--av', **_help_param(ACCESS_VECTOR))
    parser.add_argument('--ac', **_help_param(ACCESS_COMPLEXITY))
    parser.add_argument('--au', **_help_param(AUTHENTICATION))
    parser.add_argument('--c', **_help_param(CONFIDENTIALITY_IMPACT))
    parser.add_argument('--i', **_help_param(INTEGRITY_IMPACT))
    parser.add_argument('--a', **_help_param(AVAIL_IMPACT))
    parser.add_argument('-T', help="Add temporar metrics to calculation", action='store_true',
                        dest=_TEMPORAR)
    parser.add_argument('--e', **_help_param(EXPPLOITABILITY))
    parser.add_argument('--rl', **_help_param(REMEDATION_LEVEL))
    parser.add_argument('--rc', **_help_param(REPORT_CONFIDENCE))
    parser.add_argument('-E', help="Add environmental metrics to calculation", action='store_true',
                        dest=_ENVIRONMENTAL)
    parser.add_argument('--cdp', **_help_param(COLLATERAL_DMG_POTENTIAL))
    parser.add_argument('--td', **_help_param(TARGET_DISTRIBUTION))
    parser.add_argument('--cr', **_help_param(CONF_REQ))
    parser.add_argument('--ir', **_help_param(INTEG_REQ))
    parser.add_argument('--ar', **_help_param(AVAIL_REQ))


parser_init()

args = parser.parse_args()


def get_vector_value(in_val, vect):
    if in_val is None:
        return None
    if str.upper(in_val) in vect.values:
        return vect.values[str.upper(in_val)]
    else:
        raise KeyError("provided value not in possible values")


def interactive():
    """
    Method for handling interactive operation
    :return: dictionary with collected inputs
    """
    inputs = dict()
    inputs[_TEMPORAR] = False
    inputs[_ENVIRONMENTAL] = False

    def _msg(vec):
        print(_help_msg(vec))
        r_val = None
        while r_val is None:
            try:
                print("Pease, enter value for {}: \n".format(vec.name))
                in_val = str.upper(input())
                r_val = get_vector_value(in_val, vec)
            except KeyError:
                print("Invalid value")

        return r_val

    print("cvss2 parser - interactive mode")
    print("[BASE_VECTORS]")
    for vect in Base.vectors:
        inputs[vect.name] = _msg(vect)

    if input("Apply Temporar metric?[Y/n]").lower() != 'n':
        inputs[_TEMPORAR] = True
        for vect in Temporar.vectors:
            inputs[vect.name] = _msg(vect)

    if input("Apply Environmental metric?[Y/n]").lower() != 'n':
        inputs[_ENVIRONMENTAL] = True
        for vect in Environmental.vectors:
            inputs[vect.name] = _msg(vect)

    return inputs


if __name__ == "__main__":
    try:
        args = vars(parser.parse_args())
        vectors = args
        if args['Interactive']:
            vectors = interactive()

        else:
            for vector in Base.vectors:
                vectors[vector.name] = get_vector_value(vectors[vector.name], vector)
            for vector in Temporar.vectors:
                vectors[vector.name] = get_vector_value(vectors[vector.name], vector)
            for vector in Environmental.vectors:
                vectors[vector.name] = get_vector_value(vectors[vector.name], vector)

        temporary, environmental = vectors[_TEMPORAR], vectors[_ENVIRONMENTAL]
        del vectors[_TEMPORAR], vectors[_ENVIRONMENTAL]

        print("{:.1f}".format(calculate_results(vectors, temporary, environmental)))

    except KeyboardInterrupt:
        exit(0)
    except EOFError:
        exit(1)
